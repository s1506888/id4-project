#!/bin/sh
# Grid Engine options
#$-N BEAST_array
#$ -wd /exports/cmvm/eddie/eb/groups/lycett_processing/james/beast    
#$ -l h_vmem=16G
#$ -l h_rt=12:00:00 
#$ -t 1-378
 
INPUT_FILE=$(sed -n "${SGE_TASK_ID}p" listxml_2019-04-19.txt)

 ./beast.sh $INPUT_FILE
