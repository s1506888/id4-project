#!/bin/sh
cd /exports/cmvm/eddie/eb/groups/lycett_processing/james/beast

#Initialise the environment modules
. /etc/profile.d/modules.sh

#load Beast
module load roslin/beast/1.8.4
module load roslin/beagle-lib/2.1.2

#run Beast
INPUT="$1"

echo '================================'
echo '** Hello BEAST user! **'

beast $INPUT

echo '** Done **'
echo '================================'

