#!/bin/sh
# Grid Engine options
#$ -N blast_test
#$ -wd /exports/cmvm/eddie/eb/groups/lycett_processing/james
#$ -pe sharedmem 8
#$ -l h_vmem=4G
#$ -l h_rt=01:00:00
#$ -m baes
#$ -M s1506888@sms.ed.ac.uk

#Initialise the environment modules
. /etc/profile.d/modules.sh

#load Blast+
module load roslin/blast+/2.7.1

#Export database
export BLASTDB=/exports/cmvm/eddie/eb/groups/lycett_processing/james/refseq_protein

#Set input and output
INPUTNAME="$1"
OUTPUTNAME=`echo ${INPUTNAME} | awk -F "." '{print $1}'`

#Run blastx
blastx -query $INPUTNAME  -out $OUTPUTNAME.out -db refseq_protein -outfmt "6 qseqid sblastname stitle sseqid pident num_threads 8"