#!/bin/sh
# Grid Engine options 
#$ -N blast_array 
#$ -wd /exports/cmvm/eddie/eb/groups/lycett_processing/james
#$ -l h_vmem=2G
#$ -m baes
#$ -M s1506888@sms.ed.ac.uk
#$ -t 1-1200
infile=`sed -n -e "$SGE_TASK_ID p" filenames.txt`

./blast.sh $infile 
#done